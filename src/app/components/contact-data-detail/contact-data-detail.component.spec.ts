import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ContactDataDetailComponent } from './contact-data-detail.component';

describe('ContactDataDetailComponent', () => {
  let component: ContactDataDetailComponent;
  let fixture: ComponentFixture<ContactDataDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ContactDataDetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ContactDataDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
