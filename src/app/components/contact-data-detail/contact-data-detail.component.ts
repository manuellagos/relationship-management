import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-contact-data-detail',
  templateUrl: './contact-data-detail.component.html',
  styleUrls: ['./contact-data-detail.component.css']
})
export class ContactDataDetailComponent implements OnInit {

  @Input() contact : any;
  constructor() { 
  }

  ngOnInit(): void {
  }

}
