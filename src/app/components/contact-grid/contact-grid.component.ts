import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-contact-grid',
  templateUrl: './contact-grid.component.html',
  styleUrls: ['./contact-grid.component.css']
})
export class ContactGridComponent implements OnInit {

  @Input() source : any;
  constructor() { }

  ngOnInit(): void {
  }

}
