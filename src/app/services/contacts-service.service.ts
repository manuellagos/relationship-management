import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ContactsServiceService {

  constructor() {

   }

   getContacts(){
    let contacts =  [
      {
        "salutation" : "Mr",
        "firstName" : "Paul",
        "lastName" : "Mitchell",
        "addressLine1" : "1222 56th ST S",
        "addressLine2" : "apt 23",
        "city" : "Fargo",
        "state" : "ND",
        "zipCode" : "58104",
        "emailAddress" : "ash@sample.com",
        "telePhoneNumber" : "5551345699",
        "fullName" : "Mr Paul Mitchell"
      },
      {
        "salutation" : "Mr",
        "firstName" : "Jason",
        "lastName" : "White",
        "addressLine1" : "1777 56th ST S",
        "addressLine2" : "apt 7",
        "city" : "Fargo",
        "state" : "ND",
        "zipCode" : "58105",
        "emailAddress" : "Jason@sample.com",
        "telePhoneNumber" : "5551345699",
        "fullName" : "Mr Jason White"
      },
      {
        "salutation" : "Mr",
        "firstName" : "Luis",
        "lastName" : "Ratchell",
        "addressLine1" : "1888 56th ST S",
        "addressLine2" : "apt 12",
        "city" : "Fargo",
        "state" : "ND",
        "zipCode" : "58106",
        "emailAddress" : "Luis@sample.com",
        "telePhoneNumber" : "5551345699",
        "fullName" : "Mr Luis Ratchell"
      },
      {
        "salutation" : "Mr",
        "firstName" : "Rick",
        "lastName" : "Dallas",
        "addressLine1" : "1444 56th ST S",
        "addressLine2" : "apt 24",
        "city" : "Fargo",
        "state" : "ND",
        "zipCode" : "58107",
        "emailAddress" : "Rick@sample.com",
        "telePhoneNumber" : "5551345699",
        "fullName" : "Mr Rick Dallas"
      },
      {
        "salutation" : "Mr",
        "firstName" : "Diego",
        "lastName" : "Prado",
        "addressLine1" : "1333 56th ST S",
        "addressLine2" : "apt 33",
        "city" : "Fargo",
        "state" : "ND",
        "zipCode" : "58108",
        "emailAddress" : "Diego@sample.com",
        "telePhoneNumber" : "5551345699",
        "fullName" : "Mr Diego Prado"
      }
    ]
    return contacts;
  }

}
