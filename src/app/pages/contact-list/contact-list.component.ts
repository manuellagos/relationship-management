import { Component, OnInit } from '@angular/core';
import { ContactsServiceService } from 'src/app/services/contacts-service.service';

@Component({
  selector: 'app-contact-list',
  templateUrl: './contact-list.component.html',
  styleUrls: ['./contact-list.component.css']
})
export class ContactListComponent implements OnInit {

  public showDetailContactModal = false;
  public lstContacts : any ;
  constructor(private contactsServiceService: ContactsServiceService) {
   }

  ngOnInit(): void {
    this.lstContacts = this.contactsServiceService.getContacts();
  }
}
